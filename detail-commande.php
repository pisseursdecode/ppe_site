<?php
require("tpl/smarty.class.php");

$mysqli = new mysqli("89.38.148.48", "rootppe", "rootppe", "ppe_rh2");
$mysqli->set_charset("utf8");

$tpl = new Smarty();

session_start();

if (empty($_SESSION) || !isset($_GET['id_commande'])) {
	$tpl->assign('erreur', 'erreur');
}else{

	$res = $mysqli->multi_query("CALL getDetailCommande(".$_GET['id_commande'].")");
	do {
	    if ($res = $mysqli->store_result()) {
	        $detail_order = $res->fetch_all();
	        $res->free();
	    }
	} while ($mysqli->more_results() && $mysqli->next_result());


	$tpl->assign('id_commande', $_GET['id_commande']);
	$tpl->assign('order', $detail_order);
}

$tpl->display("detail-commande.tlp");

?>
