<?php /* Smarty version 3.1.27, created on 2016-02-22 09:33:32
         compiled from "C:\wamp\www\_backoffice\templates\product-edit.tlp" */ ?>
<?php
/*%%SmartyHeaderCode:1625256cac7dc461b88_96000875%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '2977f3a21b3f64f642dd5adeb405df02a5597e99' => 
    array (
      0 => 'C:\\wamp\\www\\_backoffice\\templates\\product-edit.tlp',
      1 => 1456130008,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1625256cac7dc461b88_96000875',
  'variables' => 
  array (
    'product' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_56cac7dc4a7010_22179693',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_56cac7dc4a7010_22179693')) {
function content_56cac7dc4a7010_22179693 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '1625256cac7dc461b88_96000875';
?>
<!DOCTYPE html>
<html lang="fr">

<head>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>GSB Admin - Edition du médicament N°<?php echo $_smarty_tpl->tpl_vars['product']->value['medicM_Id'];?>
</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/sb-admin.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <?php echo '<script'; ?>
 src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"><?php echo '</script'; ?>
>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">GSB Admin</a>
            </div>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li>
                        <a href="index.php"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                    </li>
                    <li>
                        <a href="list-client.php"><i class="fa fa-fw fa-users"></i> Clients</a>
                    </li>
                    <li>
                        <a href="list-collaborateur.php"><i class="fa fa-fw fa-users"></i> Collaborateurs</a>
                    </li>
                    <li>
                        <a href="list-order.php"><i class="fa fa-fw fa-shopping-cart"></i> Commandes</a>
                    </li>
                    <li>
                        <a href="list-medic.php"><i class="fa fa-fw fa-heart"></i> Medicaments</a>
                    </li>
                    <!-- <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#demo"><i class="fa fa-fw fa-arrows-v"></i> Dropdown <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="demo" class="collapse">
                            <li>
                                <a href="#">Dropdown Item</a>
                            </li>
                            <li>
                                <a href="#">Dropdown Item</a>
                            </li>
                        </ul>
                    </li> -->
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Edition du médicament N°<?php echo $_smarty_tpl->tpl_vars['product']->value['medicM_Id'];?>

                        </h1>
                    </div>
                </div>
                <!-- /.row -->

                <!-- /.row -->

                <form action="edit.php" method="post">
                <input name="id" hidden value="<?php echo $_smarty_tpl->tpl_vars['product']->value['medicM_Id'];?>
"> 
                    <label>Nom :</label>
                    <input type="text" name="name" class="form-control" value="<?php echo $_smarty_tpl->tpl_vars['product']->value['medicM_Nom'];?>
">

                    <label>Description :</label>
                    <textarea name="description" class="form-control" rows="5"><?php echo $_smarty_tpl->tpl_vars['product']->value['medicM_Description'];?>
</textarea>

                    <label>Prix : </label>
                    <div class="input-group">
                        <input name="price" type="text" class="form-control" value="<?php echo $_smarty_tpl->tpl_vars['product']->value['medicM_Prix'];?>
">
                        <span class="input-group-addon">€</span>
                    </div>
                    <br>
                    <button type="submit" class="btn btn-default btn-lg">
                        <span class="glyphicon glyphicon-edit" aria-hidden="true"></span> Editer
                    </button>
                </form>

                <!-- /.row -->
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <?php echo '<script'; ?>
 src="js/jquery.js"><?php echo '</script'; ?>
>

    <!-- Bootstrap Core JavaScript -->
    <?php echo '<script'; ?>
 src="js/bootstrap.min.js"><?php echo '</script'; ?>
>

    <!-- Morris Charts JavaScript -->
    <?php echo '<script'; ?>
 src="js/plugins/morris/raphael.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="js/plugins/morris/morris.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="js/plugins/morris/morris-data.js"><?php echo '</script'; ?>
>

    <?php echo '<script'; ?>
 src="js/dynamitable.jquery.min.js"><?php echo '</script'; ?>
>

</body>

</html>
<?php }
}
?>