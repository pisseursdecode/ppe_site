<?php /* Smarty version 3.1.27, created on 2016-02-22 09:32:22
         compiled from "C:\wamp\www\_backoffice\templates\list-order.tlp" */ ?>
<?php
/*%%SmartyHeaderCode:2226156cac79603b8a4_49049587%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'b656d3c4bfaa34f4ea4b61a45d948acb5b04683b' => 
    array (
      0 => 'C:\\wamp\\www\\_backoffice\\templates\\list-order.tlp',
      1 => 1456129940,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2226156cac79603b8a4_49049587',
  'variables' => 
  array (
    'list_order' => 0,
    'order' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_56cac79608a2d8_53425949',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_56cac79608a2d8_53425949')) {
function content_56cac79608a2d8_53425949 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '2226156cac79603b8a4_49049587';
?>
<!DOCTYPE html>
<html lang="fr">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>GSB Admin - Liste des commandes</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/sb-admin.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <?php echo '<script'; ?>
 src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"><?php echo '</script'; ?>
>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">GSB Admin</a>
            </div>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li>
                        <a href="index.php"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                    </li>
                    <li>
                        <a href="list-client.php"><i class="fa fa-fw fa-users"></i> Clients</a>
                    </li>
                    <li>
                        <a href="list-collaborateur.php"><i class="fa fa-fw fa-users"></i> Collaborateurs</a>
                    </li>
                    <li class="active">
                        <a href="list-order.php"><i class="fa fa-fw fa-shopping-cart"></i> Commandes</a>
                    </li>
                    <li>
                        <a href="list-medic.php"><i class="fa fa-fw fa-heart"></i> Medicaments</a>
                    </li>
                    <!-- <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#demo"><i class="fa fa-fw fa-arrows-v"></i> Dropdown <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="demo" class="collapse">
                            <li>
                                <a href="#">Dropdown Item</a>
                            </li>
                            <li>
                                <a href="#">Dropdown Item</a>
                            </li>
                        </ul>
                    </li> -->
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Liste des commandes
                        </h1>
                    </div>
                </div>
                <!-- /.row -->

                <!-- /.row -->

                <div class="table-responsive">
            
                <!-- Initialization 
                * js-dynamitable => dynamitable trigger (table)
                -->
                <table class="js-dynamitable     table table-bordered">
                    
                    <!-- table heading -->
                    <thead>
                    
                        <!-- Sortering
                        * js-sorter-asc => ascending sorter trigger
                        * js-sorter-desc => desending sorter trigger
                        -->
                        <tr>
                            <th>N° Commande
                                <span class="js-sorter-desc     glyphicon glyphicon-chevron-down pull-right"></span>
                                <span class="js-sorter-asc     glyphicon glyphicon-chevron-up pull-right"></span>
                             </th>
                            <th>Client
                                <span class="js-sorter-desc     glyphicon glyphicon-chevron-down pull-right"></span>
                                <span class="js-sorter-asc     glyphicon glyphicon-chevron-up pull-right"></span>
                             </th>
                            <th>Date
                                <span class="js-sorter-desc     glyphicon glyphicon-chevron-down pull-right"></span>
                                <span class="js-sorter-asc     glyphicon glyphicon-chevron-up pull-right"></span>
                             </th>
                            <th>Signature
                                <span class="js-sorter-desc     glyphicon glyphicon-chevron-down pull-right"></span>
                                <span class="js-sorter-asc     glyphicon glyphicon-chevron-up pull-right"></span>
                            </th>
                            <th>Total
                                <span class="js-sorter-desc     glyphicon glyphicon-chevron-down pull-right"></span>
                                <span class="js-sorter-asc     glyphicon glyphicon-chevron-up pull-right"></span>
                            </th>
                        </tr>
                        
                        <!-- Filtering
                        * js-filter => filter trigger (input, select)
                        -->
                        <tr>
                            <th><input class="js-filter  form-control" type="text" value=""></th>
                            <th><input class="js-filter  form-control" type="text" value=""></th>
                            <th><input class="js-filter  form-control" type="text" value=""></th>
                            <th><input class="js-filter  form-control" type="text" value=""></th>
                            <th><input class="js-filter  form-control" type="text" value=""></th>
                        </tr>
                    </thead>
                    
                    <!-- table body -->
                    <tbody>
                    <?php
$_from = $_smarty_tpl->tpl_vars['list_order']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['order'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['order']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['order']->value) {
$_smarty_tpl->tpl_vars['order']->_loop = true;
$foreach_order_Sav = $_smarty_tpl->tpl_vars['order'];
?>
                    <tr>
                        <td><?php echo $_smarty_tpl->tpl_vars['order']->value[0];?>
</td>
                        <td><?php echo $_smarty_tpl->tpl_vars['order']->value[1];?>
</td>
                        <td><?php echo $_smarty_tpl->tpl_vars['order']->value[2];?>
</td>
                        <td><?php echo $_smarty_tpl->tpl_vars['order']->value[3];?>
</td>
                        <td><?php echo $_smarty_tpl->tpl_vars['order']->value[4];?>
 €</td>
                    </tr>
                    <?php
$_smarty_tpl->tpl_vars['order'] = $foreach_order_Sav;
}
?>
                    </tbody>
                    
                </table>
            </div>
                <!-- /.row -->
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <?php echo '<script'; ?>
 src="js/jquery.js"><?php echo '</script'; ?>
>

    <!-- Bootstrap Core JavaScript -->
    <?php echo '<script'; ?>
 src="js/bootstrap.min.js"><?php echo '</script'; ?>
>

    <!-- Morris Charts JavaScript -->
    <?php echo '<script'; ?>
 src="js/plugins/morris/raphael.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="js/plugins/morris/morris.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="js/plugins/morris/morris-data.js"><?php echo '</script'; ?>
>

    <?php echo '<script'; ?>
 src="js/dynamitable.jquery.min.js"><?php echo '</script'; ?>
>

</body>

</html>
<?php }
}
?>