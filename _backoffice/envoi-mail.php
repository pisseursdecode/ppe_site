<?php
require('../fpdf/fpdf.php');


$mysqli = new mysqli("89.38.148.48", "rootppe", "rootppe", "ppe_rh2");
$mysqli->set_charset("utf8");

class PDF extends FPDF
{
	// En-tête
	function HeaderPDF($commercial, $client, $date)
	{
		$this->SetFont('Arial','',30);
		$this->Cell(80);
		$this->Cell(50,20,'Facture',1,0,'C');
		// Saut de ligne
	    $this->Ln(20);
	    $this->SetFont('Arial','',14);
		$this->Write(5,'Date : '.$date);
	    $this->Ln(20);
	    // Décalage à droite
	    $this->Write(5,'Commercial : '.$commercial[0][0]);
	    // Saut de ligne
	   	$this->Ln(20);
	    // Décalage à droite
	    $this->Write(5,'Client : '.$client[0][0]);
	    // Saut de ligne
	    $this->Ln(20);

	}

	// Tableau simple
	function BasicTable($header, $data, $total)
	{
	    // En-tête
	    foreach($header as $col)
	        $this->Cell(40,7,$col,1);
	    $this->Ln();
	    // Données
	    foreach($data as $row)
	    {
	    	unset($row[1]); // on exclu la description
	    	
	    	foreach($row as $col)
	            $this->Cell(40,6,$col,1);
	      	$this->Ln();
	    }
	    $this->Ln(20);
	    $this->Write(5,'Total : '.$total.' euros');
	}
}

$pdf = new PDF();
// Titres des colonnes
$header = array('Medicament', 'Quantite', 'Prix unitaire', 'Total');
// Chargement des données

$id_commande = $_GET['id'];

$res = $mysqli->multi_query("CALL getDetailCommande(".$id_commande.")");
	do {
	    if ($res = $mysqli->store_result()) {
	        $list_order = $res->fetch_all();
	        $res->free();
	    }
	} while ($mysqli->more_results() && $mysqli->next_result());


$res = $mysqli->multi_query("CALL getCommercialCommande(".$id_commande.")");
	do {
	    if ($res = $mysqli->store_result()) {
	        $commercial = $res->fetch_all();
	        $res->free();
	    }
	} while ($mysqli->more_results() && $mysqli->next_result());

$res = $mysqli->multi_query("CALL getClientCommande(".$id_commande.")");
	do {
	    if ($res = $mysqli->store_result()) {
	        $client = $res->fetch_all();
	        $res->free();
	    }
	} while ($mysqli->more_results() && $mysqli->next_result());

$res = $mysqli->multi_query("CALL getGeneralInfoCommande(".$id_commande.")");
	do {
	    if ($res = $mysqli->store_result()) {
	        $generalInfo = $res->fetch_all();

	        $res->free();
	    }
	} while ($mysqli->more_results() && $mysqli->next_result());


$date = $generalInfo[0][0];
$total = $generalInfo[0][1];


$pdf->AddPage();
$pdf->HeaderPDF($commercial, $client, $date);
$pdf->SetFont('Arial','',14);
$pdf->BasicTable($header,$list_order, $total);


//MAIL

$res = $mysqli->multi_query("CALL getMailClientCommande(".$id_commande.")");
	do {
	    if ($res = $mysqli->store_result()) {
	        $mailClient = $res->fetch_all();

	        $res->free();
	    }
	} while ($mysqli->more_results() && $mysqli->next_result());

$to = $mailClient[0][0]; 
$from = "noreply@gsb.com"; 
$subject = "Confirmation de votre commande n°".$id_commande; 
$message = "<p>Veuillez trouver ci-joint la facture pour votre commande n° ".$id_commande."</p>";

// a random hash will be necessary to send mixed content
$separator = md5(time());

// carriage return type (we use a PHP end of line constant)
$eol = PHP_EOL;

// attachment name
$filename = "facture.pdf";

// encode data (puts attachment in proper format)
$pdfdoc = $pdf->Output("", "S");
$attachment = chunk_split(base64_encode($pdfdoc));

// main header
$headers  = "From: ".$from.$eol;
$headers .= "MIME-Version: 1.0".$eol; 
$headers .= "Content-Type: multipart/mixed; boundary=\"".$separator."\"";

// no more headers after this, we start the body! //

$body = "--".$separator.$eol;
$body .= "Content-Transfer-Encoding: 7bit".$eol.$eol;
$body .= "This is a MIME encoded message.".$eol;

// message
$body .= "--".$separator.$eol;
$body .= "Content-Type: text/html; charset=\"iso-8859-1\"".$eol;
$body .= "Content-Transfer-Encoding: 8bit".$eol.$eol;
$body .= $message.$eol;

// attachment
$body .= "--".$separator.$eol;
$body .= "Content-Type: application/octet-stream; name=\"".$filename."\"".$eol; 
$body .= "Content-Transfer-Encoding: base64".$eol;
$body .= "Content-Disposition: attachment".$eol.$eol;
$body .= $attachment.$eol;
$body .= "--".$separator."--";

// send message
mail($to, $subject, $body, $headers);

?>