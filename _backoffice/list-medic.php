<?php
require("../tpl/smarty.class.php");

$mysqli = new mysqli("89.38.148.48", "rootppe", "rootppe", "ppe_rh2");
$mysqli->set_charset("utf8");

$tpl = new Smarty();

session_start();


if (empty($_SESSION)) {
	header('Location: index.php');
}


$res = $mysqli->multi_query("SELECT * FROM Medicament");
do {
	    if ($res = $mysqli->store_result()) {
	        $list_medic = $res->fetch_all();
	        $res->free();
	    }
	} while ($mysqli->more_results() && $mysqli->next_result());


$tpl->assign('list_medic', $list_medic);


$tpl->display("list-medic.tlp");
?>
