<?php
require("../tpl/smarty.class.php");

$mysqli = new mysqli("89.38.148.48", "rootppe", "rootppe", "ppe_rh2");
$mysqli->set_charset("utf8");

$tpl = new Smarty();

session_start();


if (empty($_SESSION)) {
	header('Location: index.php');
}

// Client
$result = $mysqli->query("SELECT count(pers_Id) AS nbr_client FROM Client");
$res = $result->fetch_object();
$tpl->assign('nbr_client', $res->nbr_client);

// collaborateur
$result = $mysqli->query("SELECT count(pers_Id) AS nbr_collabo FROM collaborateurs");
$res = $result->fetch_object();
$tpl->assign('nbr_collabo', $res->nbr_collabo);

// commande
$result = $mysqli->query("SELECT count(com_Id) AS nbr_order FROM commander");
$res = $result->fetch_object();
$tpl->assign('nbr_order', $res->nbr_order);

// medicament
$result = $mysqli->query("SELECT count(medicM_Id) AS nbr_medic FROM Medicament");
$res = $result->fetch_object();
$tpl->assign('nbr_medic', $res->nbr_medic);



$tpl->display("dashboard.tlp");
?>
