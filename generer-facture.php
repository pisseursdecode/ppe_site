<?php
require('fpdf/fpdf.php');


$mysqli = new mysqli("89.38.148.48", "rootppe", "rootppe", "ppe_rh2");
$mysqli->set_charset("utf8");

class PDF extends FPDF
{
	// En-tête
	function HeaderPDF($commercial, $client, $date)
	{
		$this->SetFont('Arial','',30);
		$this->Cell(80);
		$this->Cell(50,20,'Facture',1,0,'C');
		// Saut de ligne
	    $this->Ln(20);
	    $this->SetFont('Arial','',14);
		$this->Write(5,'Date : '.$date);
	    $this->Ln(20);
	    // Décalage à droite
	    $this->Write(5,'Commercial : '.$commercial[0][0]);
	    // Saut de ligne
	   	$this->Ln(20);
	    // Décalage à droite
	    $this->Write(5,'Client : '.$client[0][0]);
	    // Saut de ligne
	    $this->Ln(20);

	}

	// Tableau simple
	function BasicTable($header, $data, $total)
	{
	    // En-tête
	    foreach($header as $col)
	        $this->Cell(40,7,$col,1);
	    $this->Ln();
	    // Données
	    foreach($data as $row)
	    {
	    	unset($row[1]); // on exclu la description
	    	
	    	foreach($row as $col)
	            $this->Cell(40,6,$col,1);
	      	$this->Ln();
	    }
	    $this->Ln(20);
	    $this->Write(5,'Total : '.$total.' euros');
	}
}

$pdf = new PDF();
// Titres des colonnes
$header = array('Medicament', 'Quantite', 'Prix unitaire', 'Total');
// Chargement des données

$id_commande = $_GET['id'];

$res = $mysqli->multi_query("CALL getDetailCommande(".$id_commande.")");
	do {
	    if ($res = $mysqli->store_result()) {
	        $list_order = $res->fetch_all();
	        $res->free();
	    }
	} while ($mysqli->more_results() && $mysqli->next_result());


$res = $mysqli->multi_query("CALL getCommercialCommande(".$id_commande.")");
	do {
	    if ($res = $mysqli->store_result()) {
	        $commercial = $res->fetch_all();
	        $res->free();
	    }
	} while ($mysqli->more_results() && $mysqli->next_result());

$res = $mysqli->multi_query("CALL getClientCommande(".$id_commande.")");
	do {
	    if ($res = $mysqli->store_result()) {
	        $client = $res->fetch_all();
	        $res->free();
	    }
	} while ($mysqli->more_results() && $mysqli->next_result());

$res = $mysqli->multi_query("CALL getGeneralInfoCommande(".$id_commande.")");
	do {
	    if ($res = $mysqli->store_result()) {
	        $generalInfo = $res->fetch_all();

	        $res->free();
	    }
	} while ($mysqli->more_results() && $mysqli->next_result());


$date = $generalInfo[0][0];
$total = $generalInfo[0][1];


$pdf->AddPage();
$pdf->HeaderPDF($commercial, $client, $date);
$pdf->SetFont('Arial','',14);
$pdf->BasicTable($header,$list_order, $total);
$pdf->Output();
?>