<?php

class MySQL extends PDO {
 
    private $bdd, $moteur, $hote, $login, $mdp, $base;
 
    public function __construct() {
 
        $this->moteur = "mysql";
        $this->hote   = "89.38.148.48";
        $this->login  = "rootppe";
        $this->mdp    = "rootppe";
        $this->base   = "ppe_rh2";
 
        try {
	        $this->bdd = new PDO($this->moteur .':host='. $this->hote .';dbname='. $this->base, $this->login, $this->mdp);
	        $this->bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch(Exception $e) {
        	exit('Erreur : '. $e->getMessage());
       }
    }      
     
    public function Query($requete) {
    return $this->bdd->query($requete);
    }
     
    public function fetch($resultat) {
    return $resultat->fetch();
    }
     
    public function compteur($resultat) {
        if(is_object($resultat)) {
        echo 'ok';
        } else {
        echo 'pas ok';
        }
    return $resultat->rowCount();
    }
     
    public function close($resultat) {
    return $resultat->closeCursor();
    }  
}
?>