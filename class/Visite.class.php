<?php 
class  Visite{

	public $visite_Id;
	public $col_Id;
	public $note_Id;
	public $client_Id;
	public $visite_Status;
	public $visite_Etat;
	public $visite_Date;
	public $visite_DebutHeure;
	public $visite_FinHeure;

	public function Visite($visite_Id,$col_Id,$note_Id,$client_Id,$visite_Status,$visite_Etat,$visite_Date,$visite_DebutHeure,$visite_FinHeure){ 
		$this->visite_Id=$visite_Id;
		$this->col_Id=$col_Id;
		$this->note_Id=$note_Id;
		$this->client_Id=$client_Id;
		$this->visite_Status=$visite_Status;
		$this->visite_Etat=$visite_Etat;
		$this->visite_Date=$visite_Date;
		$this->visite_DebutHeure=$visite_DebutHeure;
		$this->visite_FinHeure=$visite_FinHeure;
	} 

	 /*function pour obternir les informations de Visite en fonction de l'id*/ 
	public function getinfo_Visite($id){ 
		$sql="select * from Visite where visite_Id='$this->visite_Id'"; 
		$result=mysqli_query($sql,$id);
		return $result;
	}

	 /*function pour ajouter un enregistrement dans la table Visite */
	public function ajouter_Visite($id){ 
		$sql="insert into Visite (visite_Id,col_Id,note_Id,client_Id,visite_Status,visite_Etat,visite_Date,visite_DebutHeure,visite_FinHeure) VALUES ('$this->visite_Id','$this->col_Id','$this->note_Id','$this->client_Id','$this->visite_Status','$this->visite_Etat','$this->visite_Date','$this->visite_DebutHeure','$this->visite_FinHeure');"; 
		$result=mysqli_query($sql,$id);
		if($result){
			return true;
		}else return false;
	}

	 /*squellette function pour modifier un enregistrement dans la table Visite */
	public function modifier_Visite($id){ 
		$sql=""; 
		$result=mysqli_query($sql,$id);
		if($result){
			return true;
		}else return false;
	}

	 /*function pour supprimer un enregistrement dans la table Visite */
	public function supprimer_Visite($id){ 
		$sql="delete from Visite where visite_Id='$this->visite_Id'"; 
		$result=mysqli_query($sql,$id);
		if($result){
			return true;
		}else return false;
	}

}