<?php 
class  Personne{

	public $pers_Id;
	public $pers_Nom;
	public $pers_Prenom;
	public $pers_Adresse;
	public $pers_Compement;
	public $pers_Ville;
	public $pers_Cp;
	public $pers_Longitude;
	public $pers_Latitude;
	public $pers_Tel;
	public $pers_Mail;
	public $region_Id;

	public function Personne($pers_Id,$pers_Nom,$pers_Prenom,$pers_Adresse,$pers_Compement,$pers_Ville,$pers_Cp,$pers_Longitude,$pers_Latitude,$pers_Tel,$pers_Mail,$region_Id){ 
		$this->pers_Id=$pers_Id;
		$this->pers_Nom=$pers_Nom;
		$this->pers_Prenom=$pers_Prenom;
		$this->pers_Adresse=$pers_Adresse;
		$this->pers_Compement=$pers_Compement;
		$this->pers_Ville=$pers_Ville;
		$this->pers_Cp=$pers_Cp;
		$this->pers_Longitude=$pers_Longitude;
		$this->pers_Latitude=$pers_Latitude;
		$this->pers_Tel=$pers_Tel;
		$this->pers_Mail=$pers_Mail;
		$this->region_Id=$region_Id;
	} 

	 /*function pour obternir les informations de Personne en fonction de l'id*/ 
	public function getinfo_Personne($id){ 
		$sql="select * from Personne where pers_Id='$this->pers_Id'"; 
		$result=mysqli_query($sql,$id);
		return $result;
	}

	 /*function pour ajouter un enregistrement dans la table Personne */
	public function ajouter_Personne($id){ 
		$sql="insert into Personne (pers_Id,pers_Nom,pers_Prenom,pers_Adresse,pers_Compement,pers_Ville,pers_Cp,pers_Longitude,pers_Latitude,pers_Tel,pers_Mail,region_Id) VALUES ('$this->pers_Id','$this->pers_Nom','$this->pers_Prenom','$this->pers_Adresse','$this->pers_Compement','$this->pers_Ville','$this->pers_Cp','$this->pers_Longitude','$this->pers_Latitude','$this->pers_Tel','$this->pers_Mail','$this->region_Id');"; 
		$result=mysqli_query($sql,$id);
		if($result){
			return true;
		}else return false;
	}

	 /*squellette function pour modifier un enregistrement dans la table Personne */
	public function modifier_Personne($id){ 
		$sql=""; 
		$result=mysqli_query($sql,$id);
		if($result){
			return true;
		}else return false;
	}

	 /*function pour supprimer un enregistrement dans la table Personne */
	public function supprimer_Personne($id){ 
		$sql="delete from Personne where pers_Id='$this->pers_Id'"; 
		$result=mysqli_query($sql,$id);
		if($result){
			return true;
		}else return false;
	}

}