<?php
     /***************************************************************************************************************
      *																												*
	  *   					g�n�ration automatique de fichiers.class												*
	  *						houndegnonm@gmail.com																	*
	  *																												*
      ***************************************************************************************************************/
    include_once("Connexion.class.php");
	//remplacer ici localite par le nom de la table � g�n�r�e
	//#########################################################
	//#########################################################

		genere_class("Client");
		
	//#########################################################
	//#########################################################

	function genere_class($table_name){
		//connexion � la base de donn�es
		$adminConnexion= new Connexion();
		$i=$adminConnexion->connect_server();
		$i1=$adminConnexion->connect_db($i);
		if($i1){
			//initialisation variable qui stock le contenu du fichier � g�n�rer
			$ligne="";
			//liste des colonnes des colonnes d'une table
			$req_champ = "SHOW COLUMNS FROM $table_name";
			$result_champ = mysqli_query($req_champ) or die ("Erreur lors de la recuperation du nombre d'entrees");
			$nbre_champ= mysqli_num_rows($result_champ);
			$c=0;
			$champ=array();
			while($row= mysqli_fetch_object($result_champ)){
					$champ[$c]=$row->Field;
					$c++;
			}
			//g�n�ration du fichier $table.class.php
			$file_class= fopen($table_name.".class.php","w");
			//d�but de g�n�ation
			$ligne.="<?php \n";
			$ligne.="class  $table_name{\n\n";
			//g�n�ration des attributs de la classe
			for($c=0;$c<count($champ);$c++)
				$ligne.="\tpublic $".$champ[$c].";\n";
			//g�n�ration du contructeur de la classe
			$ligne.="\n\tpublic function $table_name($". implode(",$",$champ) ."){ \n";
			for($c=0;$c<count($champ);$c++)
				$ligne.="\t\t$"."this->".$champ[$c]."=$".$champ[$c].";\n";
			$ligne.="\t} \n\n";
			/*function pour obternir les informations de $table_name en fonction de l'id*/
			//function getinfo
			$ligne.="\t /*function pour obternir les informations de $table_name en fonction de l'id*/ \n";
			$ligne.="\tpublic function getinfo_$table_name($"."id){ \n";
			$ligne.="\t\t$"."sql=\"select * from $table_name where ".$champ[0]."='$"."this->".$champ[0]."'\"; \n";
			$ligne.="\t\t$"."result=mysqli_query($"."sql,$"."id);\n";
			$ligne.="\t\treturn $"."result;\n";
			$ligne.="\t}\n\n";
			$ligne.="\t /*function pour ajouter un enregistrement dans la table $table_name */\n";
			/*function pour ajouter un enregistrement dans la table $table_name */
			//function add
			$ligne.="\tpublic function ajouter_$table_name($"."id){ \n";
			$ligne.="\t\t$"."sql=\"insert into $table_name (".implode(",",$champ).") VALUES ('$"."this->".implode("','$"."this->",$champ)."');\"; \n";
			$ligne.="\t\t$"."result=mysqli_query($"."sql,$"."id);\n";
			$ligne.="\t\tif($"."result){\n";
			$ligne.="\t\t\treturn true;\n";
			$ligne.="\t\t}else return false;\n";
			$ligne.="\t}\n\n";
			$ligne.="\t /*squellette function pour modifier un enregistrement dans la table $table_name */\n";
			/*squellette de function pour modifier un enregistrement dans la table $table_name */
			//function update
			$ligne.="\tpublic function modifier_$table_name($"."id){ \n";
			$ligne.="\t\t$"."sql=\"\"; \n";
			$ligne.="\t\t$"."result=mysqli_query($"."sql,$"."id);\n";
			$ligne.="\t\tif($"."result){\n";
			$ligne.="\t\t\treturn true;\n";
			$ligne.="\t\t}else return false;\n";
			$ligne.="\t}\n\n";
			$ligne.="\t /*function pour supprimer un enregistrement dans la table $table_name */\n";
			/*function pour supprimer un enregistrement dans la table $table_name */
			//function delete
			$ligne.="\tpublic function supprimer_$table_name($"."id){ \n";
			$ligne.="\t\t$"."sql=\"delete from $table_name where ".$champ[0]."='$"."this->".$champ[0]."'\"; \n";
			$ligne.="\t\t$"."result=mysqli_query($"."sql,$"."id);\n";
			$ligne.="\t\tif($"."result){\n";
			$ligne.="\t\t\treturn true;\n";
			$ligne.="\t\t}else return false;\n";
			$ligne.="\t}\n\n";
			//fin
			$ligne.="}";
			fwrite($file_class,$ligne);
		}
	}
?>