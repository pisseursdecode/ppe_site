<?php
header('Content-Type: text/html; charset=utf-8');
$mysqli = mysqli_connect('89.38.148.48', 'rootppe', 'rootppe', 'ppe_rh2');

if ($mysqli->connect_error) {
    die('Connect Error (' . $mysqli->connect_errno . ') ' . $mysqli->connect_errno);
} else {
    
    function utf8_string_array_encode(&$array)
    {
        $func = function(&$value, &$key)
        {
            if (is_string($value)) {
                $value = utf8_encode($value);
            }
            if (is_string($key)) {
                $key = utf8_encode($key);
            }
            if (is_array($value)) {
                utf8_string_array_encode($value);
            }
        };
        array_walk($array, $func);
        return $array;
    }
    $tagRequest = $_POST['tag'];
    $request    = $_POST['request'];

    $decodeRequest = base64_decode($request);
    $sth           = $mysqli->query($decodeRequest);
    
    if ($tagRequest == 1) { // Query
        $rows          = array();
        while ($r = mysqli_fetch_assoc($sth)) {
            $rows[] = utf8_string_array_encode($r);
        }
        print json_encode($rows);
    }
}
error_reporting(E_ALL);
?>
