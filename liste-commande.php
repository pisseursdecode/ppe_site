<?php
require("tpl/smarty.class.php");

$mysqli = new mysqli("89.38.148.48", "rootppe", "rootppe", "ppe_rh2");
$mysqli->set_charset("utf8");

$tpl = new Smarty();

session_start();

if (empty($_SESSION)) {
	$tpl->assign('erreur', 'erreur');
}else {
	$res = $mysqli->multi_query("CALL getAllOrderClient(".$_SESSION['id'].")");
	do {
	    if ($res = $mysqli->store_result()) {
	        $list_order = $res->fetch_all();
	        $res->free();
	    }
	} while ($mysqli->more_results() && $mysqli->next_result());

	$tpl->assign('order', $list_order);

}

$tpl->display("liste-commande.tlp");

?>
