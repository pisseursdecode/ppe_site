<?php /* Smarty version 3.1.27, created on 2016-02-22 09:06:44
         compiled from "C:\wamp\www\templates\liste-commande.tlp" */ ?>
<?php
/*%%SmartyHeaderCode:712856cac194bd88b5_24265515%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'acabe82a0a41d11c81a3a5c39970b2be0e74a369' => 
    array (
      0 => 'C:\\wamp\\www\\templates\\liste-commande.tlp',
      1 => 1456128307,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '712856cac194bd88b5_24265515',
  'variables' => 
  array (
    'erreur' => 0,
    'order' => 0,
    'single_order' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_56cac194c316f4_22408000',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_56cac194c316f4_22408000')) {
function content_56cac194c316f4_22408000 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '712856cac194bd88b5_24265515';
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Dynamitable</title>
        <link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css" rel="stylesheet">
        <style>
            <!--
            .glyphicon {
                cursor: pointer;
            }
            
            input, select{
                width: 100%;
            }
            
            .second, .glyphicon-chevron-down, .glyphicon-chevron-up{
                color: red;
            }

            -->
        </style>
    </head>
    <body>



        <div class="col-xs-12  col-sm-12 col-md-10 col-md-offset-1 col-lg-10  col-lg-offset-1">
        <?php if (isset($_smarty_tpl->tpl_vars['erreur']->value)) {?>
            <div class="jumbotron">
                <div class="container">
                    <h1>:(</h1>
                    <p>Vous ne pouvez pas accéder à cette page sans être connecté.</p>
                    <p><a class="btn btn-primary btn-lg" href="index.php" role="button">Se connecter</a></p>
                </div>
            </div>
        <?php } else { ?>
            <h1><span class="first">Liste des Commandes</span></h1>
            
            <div class="table-responsive">
            
                <!-- Initialization 
                * js-dynamitable => dynamitable trigger (table)
                -->
                <table class="js-dynamitable     table table-bordered">
                    
                    <!-- table heading -->
                    <thead>
                    
                        <!-- Sortering
                        * js-sorter-asc => ascending sorter trigger
                        * js-sorter-desc => desending sorter trigger
                        -->
                        <tr>
                            <th>N° Commande
                                <span class="js-sorter-desc     glyphicon glyphicon-chevron-down pull-right"></span>
                                <span class="js-sorter-asc     glyphicon glyphicon-chevron-up pull-right"></span>
                             </th>
                            <th>Collaborateur
                                <span class="js-sorter-desc     glyphicon glyphicon-chevron-down pull-right"></span>
                                <span class="js-sorter-asc     glyphicon glyphicon-chevron-up pull-right"></span>
                             </th>
                            <th>Date
                                <span class="js-sorter-desc     glyphicon glyphicon-chevron-down pull-right"></span>
                                <span class="js-sorter-asc     glyphicon glyphicon-chevron-up pull-right"></span>
                             </th>
                            <th>Signature
                                <span class="js-sorter-desc     glyphicon glyphicon-chevron-down pull-right"></span>
                                <span class="js-sorter-asc     glyphicon glyphicon-chevron-up pull-right"></span>
                            </th>
                            <th>Total
                                <span class="js-sorter-desc     glyphicon glyphicon-chevron-down pull-right"></span>
                                <span class="js-sorter-asc     glyphicon glyphicon-chevron-up pull-right"></span>
                            </th>
                        </tr>
                        
                        <!-- Filtering
                        * js-filter => filter trigger (input, select)
                        -->
                        <tr>
                            <th><input class="js-filter  form-control" type="text" value=""></th>
                            <th><input class="js-filter  form-control" type="text" value=""></th>
                            <th><input class="js-filter  form-control" type="text" value=""></th>
                            <th><input class="js-filter  form-control" type="text" value=""></th>
                            <th><input class="js-filter  form-control" type="text" value=""></th>
                        </tr>
                    </thead>
                    
                    <!-- table body -->
                    <tbody>
                    <?php
$_from = $_smarty_tpl->tpl_vars['order']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['single_order'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['single_order']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['single_order']->value) {
$_smarty_tpl->tpl_vars['single_order']->_loop = true;
$foreach_single_order_Sav = $_smarty_tpl->tpl_vars['single_order'];
?>
                    <tr>
                        <td><a href="detail-commande.php?id_commande=<?php echo $_smarty_tpl->tpl_vars['single_order']->value[0];?>
"><?php echo $_smarty_tpl->tpl_vars['single_order']->value[0];?>
</a></td>
                        <td><?php echo $_smarty_tpl->tpl_vars['single_order']->value[1];?>
</td>
                        <td><?php echo $_smarty_tpl->tpl_vars['single_order']->value[2];?>
</td>
                        <td><?php echo $_smarty_tpl->tpl_vars['single_order']->value[3];?>
</td>
                        <td><?php echo $_smarty_tpl->tpl_vars['single_order']->value[4];?>
 €</td>
                    </tr>
                    <?php
$_smarty_tpl->tpl_vars['single_order'] = $foreach_single_order_Sav;
}
?>
                    </tbody>
                    
                </table>
            </div>
            <button onclick="location.href = 'index.php';" type="button" class="btn btn-danger">Deconnexion</button>
            <?php }?>
        </div>
        
        <!-- jquery -->
        <?php echo '<script'; ?>
 src="http://code.jquery.com/jquery-1.11.3.min.js"><?php echo '</script'; ?>
>
        
        <!-- dynamitable -->
        <?php echo '<script'; ?>
 src="js/dynamitable.jquery.min.js"><?php echo '</script'; ?>
>
    </body>
</html>
<?php }
}
?>