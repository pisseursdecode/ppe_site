<?php /* Smarty version 3.1.27, created on 2016-04-17 20:24:35
         compiled from "C:\wamp64\www\templates\index.tlp" */ ?>
<?php
/*%%SmartyHeaderCode:280615713f10359a5d5_27891181%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '4c3c063c92deb00e3241926b073b5a10adf3a305' => 
    array (
      0 => 'C:\\wamp64\\www\\templates\\index.tlp',
      1 => 1460924664,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '280615713f10359a5d5_27891181',
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_5713f1035e57d3_45265956',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_5713f1035e57d3_45265956')) {
function content_5713f1035e57d3_45265956 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '280615713f10359a5d5_27891181';
?>
<!DOCTYPE html>
<html lang="fr">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>GSB - Login</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/style.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <?php echo '<script'; ?>
 src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"><?php echo '</script'; ?>
>
    <![endif]-->

    <!-- jQuery -->
    <?php echo '<script'; ?>
 src="js/jquery.js"><?php echo '</script'; ?>
>

</head>

<body>

<div class="container">
    <div class="row">
        <div class="col-sm-6 col-md-4 col-md-offset-4">
            <h1 class="text-center login-title">Connectez vous</h1>
            <div class="account-wall">
                <img src="img/gsb.png" style="display: block; height: 96px; margin: 0 auto 10px; width: 96px;" 
                    alt="">
                <form class="form-signin" id="form-signin">
                <input type="email" class="form-control" placeholder="Mail" required autofocus id="mail">
                <input type="password" class="form-control" placeholder="Mot de passe" required id="mdp">
                <button class="btn btn-lg btn-primary btn-block" type="submit">Se connecter</button>
                <div id="status"></div>
                </form>
            </div>
            <a href="#" class="text-center new-account">Créer un compte</a>
        </div>
    </div>
</div>


<?php echo '<script'; ?>
 type="text/javascript">
    $("#form-signin").submit( function() {    // à la soumission du formulaire                     
        $.ajax({ // fonction permettant de faire de l'ajax
           type: "POST", // methode de transmission des données au fichier php
           url: "ajax-login.php", // url du fichier php
           data: "mail="+$("#mail").val()+"&mdp="+$("#mdp").val(), // données à transmettre
           success: function(msg){ // si l'appel a bien fonctionné
                if(msg==1) // si la connexion en php a fonctionnée
                {
                    $("#status").html("<div class='alert alert-success' role='alert'>Vous &ecirc;tes maintenant connect&eacute.</div>");
                    window.setTimeout("location=('liste-commande.php');",2000);
                    // on désactive l'affichage du formulaire et on affiche un message de bienvenue à la place
                }
                else // si la connexion en php n'a pas fonctionnée
                {
                    $("#status").html("<div class='alert alert-danger' role='alert'>Erreur lors de la connexion, veuillez v&eacute;rifier votre login et votre mot de passe.</div>");
                    // on affiche un message d'erreur dans le span prévu à cet effet
                }
           }
        });
        return false; // permet de rester sur la même page à la soumission du formulaire
    });
<?php echo '</script'; ?>
>

    <!-- Bootstrap Core JavaScript -->
    <?php echo '<script'; ?>
 src="js/bootstrap.min.js"><?php echo '</script'; ?>
>

</body>

</html>
<?php }
}
?>