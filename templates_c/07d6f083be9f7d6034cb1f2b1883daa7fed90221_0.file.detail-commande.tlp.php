<?php /* Smarty version 3.1.27, created on 2016-04-17 20:37:56
         compiled from "C:\wamp64\www\templates\detail-commande.tlp" */ ?>
<?php
/*%%SmartyHeaderCode:296645713f424490b88_89843382%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '07d6f083be9f7d6034cb1f2b1883daa7fed90221' => 
    array (
      0 => 'C:\\wamp64\\www\\templates\\detail-commande.tlp',
      1 => 1460924664,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '296645713f424490b88_89843382',
  'variables' => 
  array (
    'erreur' => 0,
    'id_commande' => 0,
    'order' => 0,
    'single_order' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_5713f4244e19b0_34259751',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_5713f4244e19b0_34259751')) {
function content_5713f4244e19b0_34259751 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '296645713f424490b88_89843382';
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Dynamitable</title>
        <link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css" rel="stylesheet">
        <style>
            <!--
            .glyphicon {
                cursor: pointer;
            }
            
            input, select{
                width: 100%;
            }
            
            .second, .glyphicon-chevron-down, .glyphicon-chevron-up{
                color: red;
            }

            a:hover,a:focus{
                background:rgba(0,0,0,.4);
                box-shadow:0 1px 0 rgba(255,255,255,.4);
            }

            a:hover span, a:focus span{
                transform:scale(1) rotate(0);   
                opacity:1;
            }


a span::before {
    border-bottom: 6px solid rgba(0, 0, 0, 0.9);
    border-left: 6px solid transparent;
    border-right: 6px solid transparent;
    content: "";
    height: 0;
    left: 10px;
    position: absolute;
    top: -6px;
    width: 0;
}

a span {
    background: rgba(0, 0, 0, 0.9) none repeat scroll 0 0;
    border-radius: 3px;
    box-shadow: 0 0 2px rgba(0, 0, 0, 0.5);
    color: #09c;
    margin-left: -35px;
    margin-top: 23px;
    opacity: 0;
    padding: 15px;
    position: absolute;
    transform: scale(0) rotateZ(-12deg);
    transition: all 0.25s ease 0s;
}


            -->
        </style>
    </head>
    <body>

        <div class="col-xs-12  col-sm-12 col-md-10 col-md-offset-1 col-lg-10  col-lg-offset-1">
        <?php if (isset($_smarty_tpl->tpl_vars['erreur']->value)) {?>
            <div class="jumbotron">
                <div class="container">
                    <h1>:(</h1>
                    <p>Vous ne pouvez pas accéder à cette page sans être connecté.</p>
                    <p><a class="btn btn-primary btn-lg" href="index.php" role="button">Se connecter</a></p>
                </div>
            </div>
        <?php } else { ?>
            <h1><span class="first">Detail de la commande n°<?php echo $_smarty_tpl->tpl_vars['id_commande']->value;?>
</span></h1>
            
            <div class="table-responsive">
            
                <!-- Initialization 
                * js-dynamitable => dynamitable trigger (table)
                -->
                <table class="js-dynamitable     table table-bordered">
                    
                    <!-- table heading -->
                    <thead>
                    
                        <!-- Sortering
                        * js-sorter-asc => ascending sorter trigger
                        * js-sorter-desc => desending sorter trigger
                        -->
                        <tr>
                            <th>Nom
                                <span class="js-sorter-desc     glyphicon glyphicon-chevron-down pull-right"></span>
                                <span class="js-sorter-asc     glyphicon glyphicon-chevron-up pull-right"></span>
                             </th>
                            <th>Quantité
                                <span class="js-sorter-desc     glyphicon glyphicon-chevron-down pull-right"></span>
                                <span class="js-sorter-asc     glyphicon glyphicon-chevron-up pull-right"></span>
                             </th>
                            <th>Prix unitaire
                                <span class="js-sorter-desc     glyphicon glyphicon-chevron-down pull-right"></span>
                                <span class="js-sorter-asc     glyphicon glyphicon-chevron-up pull-right"></span>
                             </th>
                            <th>Prix total
                                <span class="js-sorter-desc     glyphicon glyphicon-chevron-down pull-right"></span>
                                <span class="js-sorter-asc     glyphicon glyphicon-chevron-up pull-right"></span>
                            </th>
                        </tr>
                        
                        <!-- Filtering
                        * js-filter => filter trigger (input, select)
                        -->
                        <tr>
                            <th><input class="js-filter  form-control" type="text" value=""></th>
                            <th><input class="js-filter  form-control" type="text" value=""></th>
                            <th><input class="js-filter  form-control" type="text" value=""></th>
                            <th><input class="js-filter  form-control" type="text" value=""></th>
                        </tr>
                    </thead>
                    
                    <!-- table body -->
                    <tbody>
                    <?php
$_from = $_smarty_tpl->tpl_vars['order']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['single_order'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['single_order']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['single_order']->value) {
$_smarty_tpl->tpl_vars['single_order']->_loop = true;
$foreach_single_order_Sav = $_smarty_tpl->tpl_vars['single_order'];
?>
                    <tr>
                        <td><a href="#"><?php echo $_smarty_tpl->tpl_vars['single_order']->value[0];?>
<span><?php echo $_smarty_tpl->tpl_vars['single_order']->value[1];?>
</span></a></td>
                        <td><?php echo $_smarty_tpl->tpl_vars['single_order']->value[2];?>
</td>
                        <td><?php echo $_smarty_tpl->tpl_vars['single_order']->value[3];?>
 €</td>
                        <td><?php echo $_smarty_tpl->tpl_vars['single_order']->value[4];?>
 €</td>
                    </tr>
                    <?php
$_smarty_tpl->tpl_vars['single_order'] = $foreach_single_order_Sav;
}
?>
                    </tbody>
                    
                </table>
            </div>
            <button onclick="location.href = 'liste-commande.php';" type="button" class="btn btn-info">Retour</button>
            <?php }?>
        </div>


        
        <!-- jquery -->
        <?php echo '<script'; ?>
 src="http://code.jquery.com/jquery-1.11.3.min.js"><?php echo '</script'; ?>
>
        
        <!-- dynamitable -->
        <?php echo '<script'; ?>
 src="js/dynamitable.jquery.min.js"><?php echo '</script'; ?>
>
    </body>
</html>
<?php }
}
?>